<!DOCTYPE html>
<html>
<head>
    <title>Resgister | Daily Report System</title>
    <meta name="viewport" content="width=1360px, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.css">
</head>
<body>
    <section>
        <p class="welcome">New Account | Daily Report System</p>
        <div class="container">
            <?php echo $this->session->flashdata('info_user');?>
            <form method="POST" id="userForm" class="form-signin" action="<?php echo base_url('register/registration');?>"  enctype="multipart/form-data" role="form">
                <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="" required>
                <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" value="" required>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" value="" required>
                           
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" required>
                <input type="password" class="form-control" id="confirmpassword" placeholder="Confirm Password" name="confirmpassword" value="" required>
                <select class="form-control" name="role" id="user_level" tabindex="-1" required>
                  <option value=""> Select Role</option>
                  <option value="web developer">Web Developer</option>
                  <option value="web designe">Web Designe</option>
                  <option value="human resource">Human Resource</option>
                  <option value="trainee">Trainee</option>
                </select>  
                <br>   
                <a class="btn btn-default" href="<?php echo site_url(); ?>login" role="button"> Login</a> 
                <button type="submit" id="buttonID" class="btn btn-success">Register</button>    
            </form>
            <br />
            <div>
              <p style="text-align: center;"><strong>Copyright &copy; <?php echo date('Y'); ?> Daily Report System | developed by <a href="http://ptcerise.com" target="blank">Cerise</a>.</strong> All rights reserved.</p>
            </div>
        </div>
    </section>
    <footer>
        <div class="line"></div>
    </footer>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#username').blur(function(){
      var username = $('#username').val();
      var data = 'username='+username;
      $.ajax({
        type: "POST",
        data: data,
        url: "<?php echo base_url('register/check') ?>",
        dataType: "json",
        success: function(result){
          if( result.status == 1)
          {
            alert('Username already exist! Please try another username');
            $('#username').val('');
            $('#username').focus();
          }
        }
      });
    });
    $('#buttonID').click(function(e){
      var password = $('#password').val();
      var confirmpassword = $('#confirmpassword').val();
      if (password != confirmpassword){
        alert('Please enter the same password as above');
        e.preventDefault();
      }else{
        
      }
    });
  });
</script>