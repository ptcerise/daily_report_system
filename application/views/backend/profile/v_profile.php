<div class="">
  <div class="page-title">
    <div class="title_left">
    <h3></h3>
    </div>

    <div class="title_right">
      <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <!-- <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
          </span>
        </div> -->
      </div>
    </div>
  </div>

  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Account Setting <small><i>Daily Report System</i></small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
            <?php echo $this->session->flashdata('alert');?>
            <div class="profile_img">
              <!-- end of image cropping -->
              <div id="crop-avatar">
                <!-- Current avatar -->
                <img class="img-responsive avatar-view" src="<?php echo base_url('assets/upload/user_image/'.$user->user_image); ?>" alt="Avatar" title="Change the avatar">

                <!-- Cropping modal -->
                <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
                        <div class="modal-header">
                          <button class="close" data-dismiss="modal" type="button">&times;</button>
                          <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
                        </div>
                        <div class="modal-body">
                          <div class="avatar-body">
                            <!-- Upload image and data -->
                            <div class="avatar-upload">
                              <input class="avatar-src" name="avatar_src" type="hidden">
                              <input class="avatar-data" name="avatar_data" type="hidden">
                              <label for="avatarInput">Local upload</label>
                              <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
                            </div>

                            <!-- Crop and preview -->
                            <div class="row">
                              <div class="col-md-9">
                                <div class="avatar-wrapper"></div>
                              </div>
                              <div class="col-md-3">
                                <div class="avatar-preview preview-lg"></div>
                                <div class="avatar-preview preview-md"></div>
                                <div class="avatar-preview preview-sm"></div>
                              </div>
                            </div>

                            <div class="row avatar-btns">
                              <div class="col-md-9">
                                <div class="btn-group">
                                  <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>
                                  <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
                                  <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
                                  <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
                                </div>
                                <div class="btn-group">
                                  <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>
                                  <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
                                  <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
                                  <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <button class="btn btn-primary btn-block avatar-save" type="submit">Done</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- <div class="modal-footer">
                                          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                                        </div> -->
                      </form>
                    </div>
                  </div>
                </div>
                <!-- /.modal -->

                <!-- Loading state -->
                <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
              </div>
              <!-- end of image cropping -->
            </div>
            <h3><?php echo strtoupper($this->session->userdata('name')); ?></h3>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-md"><i class="fa fa-edit m-right-xs"></i>Change Avatar</button>
            <br />
            <!-- modals body -->
            <div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Change Avatar</h4>
                  </div>
                  <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/Profile/change_avatar');?>" class="form-horizontal" role="form">
                    <div class="modal-body">                    
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Image</label>
                          <div class="col-sm-10">
                            <img src="<?php echo base_url('assets/upload/user_image/thumbnail/'.$user->user_image);?>" alt="design">
                            <br>
                            <input type="file" name="avatar" accept="image/gif, image/x-png, image/jpeg" required>
                            <input type="hidden" name="id" value="<?php echo $user->user_id ?>">
                            <p class="help-block">Image size at least 400x600 px (portrait)</p>
                          </div>
                        </div>                          
                    </div>
                    <div class="modal-footer">                         
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>   
                      <button type="submit" class="btn btn-success">Save</button>
                    </div>
                  </form> 
                </div>
              </div>
            </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Edit Profile</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="password-tab3" data-toggle="tab" aria-expanded="false">Change Password</a>
                </li>
                <!-- <li role="presentation" class=""><a href="#tab_content3" id="work" role="tab" data-toggle="tab" aria-expanded="true">Projects Worked on</a>
                </li> -->
              </ul>
              <div id="myTabContent" class="tab-content">
                <div role="tabpane1" class="tab-pane fade active in" id="tab_content1" aria-labelledby="profile-tab">
                  <?php $message = $this->session->flashdata('account'); if(@$message!='') : ?>
                  <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo $message;?>
                  </div>
                  <?php endif ?>
                  <form method="POST" action="<?php echo base_url('backend/profile/account/'.$user->user_id);?>" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" value="<?php echo $user->username ?>" required readonly name="username" id="username" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Full Name <span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="name" name="name" value="<?php echo $user->name ?>" required name="name" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Phone</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="phone" value="<?php echo $user->phone ?>" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div role="tabpane2" class="tab-pane fade" id="tab_content2" aria-labelledby="password-tab">
                  <?php $message = $this->session->flashdata('password'); if(@$message!='') : ?>
                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo $message;?>
                  </div>
                  <?php endif ?>
                  <form id="demo-form2" method="post" action="<?php echo base_url('backend/profile/password/'.$user->user_id);?>" data-parsley-validate class="form-horizontal form-label-left">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Current Password <span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" class="form-control"  placeholder="Current Password" required name="current" id="current" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">New Password <span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" class="form-control"  placeholder="New Password" required name="new" id="new" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input class="form-control col-md-7 col-xs-12" type="password" placeholder="Confirm Password" class="form-control" required name="confirm" id="confirm" required  onInput="check(this);">
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div role="tabpane3" class="tab-pane fade" id="tab_content3" aria-labelledby="work">
                  <!-- start user projects -->
                  <table class="data table table-striped no-margin">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Project Name</th>
                        <th>Client Company</th>
                        <th class="hidden-phone">Hours Spent</th>
                        <th>Contribution</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>New Company Takeover Review</td>
                        <td>Deveint Inc</td>
                        <td class="hidden-phone">18</td>
                        <td class="vertical-align-mid">
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" data-transitiongoal="35"></div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>New Partner Contracts Consultanci</td>
                        <td>Deveint Inc</td>
                        <td class="hidden-phone">13</td>
                        <td class="vertical-align-mid">
                          <div class="progress">
                            <div class="progress-bar progress-bar-danger" data-transitiongoal="15"></div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Partners and Inverstors report</td>
                        <td>Deveint Inc</td>
                        <td class="hidden-phone">30</td>
                        <td class="vertical-align-mid">
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" data-transitiongoal="45"></div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>New Company Takeover Review</td>
                        <td>Deveint Inc</td>
                        <td class="hidden-phone">28</td>
                        <td class="vertical-align-mid">
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" data-transitiongoal="75"></div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- end user projects -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>        
    </div>
  </div>
</div>