<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Aktivasi Member</h4>
    </div>
    <form method="POST" id="userForm" class="form-horizontal" action="<?php echo base_url('backend/member/save_status/'.$member->user_id);?>"  enctype="multipart/form-data" role="form">
      <div class="modal-body">
        <table class="table">
          <div class="col-md-12">
            <div class="form-group">
              <label class="col-md-4 control-label">Status</label>
              <div class="col-md-6">
                <input type="hidden" name="user_id" value="<?php echo $member->user_id;?>">
                <select class="form-control" name="user_status" id="user_status" tabindex="-1" required>
                  <option value="0" <?php if ($member->user_status == '0') {echo "selected";} ?>>Not Aktive</option>
                  <option value="1" <?php if ($member->user_status == '1') {echo "selected";} ?>>Aktive</option>
                </select> 
              </div>
            </div>
          </div>
        </table>
      </div>
      <div class="modal-footer">                         
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>   
        <button type="submit" id="buttonID" class="btn btn-success">Save</button>
      </div>
    </form>
  </div>
</div>