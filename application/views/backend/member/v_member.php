<div class="">
	<div class="page-title">
        <div class="title_left">
            <h3></h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
               <!--  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for..."/>
                    <span class="input-group-btn">
                         <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div> -->
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

          <div class="x_title">
            <h2>List Member <small><i>Daily Report System</i></small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li> <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-md"> <i class="fa fa-edit m-right-xs"></i> Add Member</button></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
            <?php echo $this->session->flashdata('info_user');?>     

            <div class="modal fade bs-example-modal-md" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add New Member</h4>
                  </div>
                  <form method="POST" id="userForm" class="form-horizontal" action="<?php echo base_url('backend/member/add');?>"  enctype="multipart/form-data" role="form">
                    <div class="modal-body">
                      <table class="table">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-4 control-label">Username</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4 control-label">Full Name</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" value="" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4 control-label">Phone Number</label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" value="" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                              <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                              <input type="password" class="form-control" id="confirmpassword" placeholder="Confirm Password" name="confirmpassword" value="" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4 control-label">Role</label>
                            <div class="col-md-6">
                              <select class="form-control" name="role" id="user_level" tabindex="-1" required>
                                <option value=""> Select Role</option>
                                <option value="web developer">Web Developer</option>
                                <option value="web designe">Web Designe</option>
                                <option value="human resource">Human Resource</option>
                                <option value="trainee">Trainee</option>
                              </select> 
                            </div>
                          </div>
                          <!-- <div class="form-actions fluid">
                            <div class="col-md-6 col-md-offset-4 pull-left">
                              <button data-dismiss="modal" class="btn btn-default">Cancel</button>
                              <button type="submit" id="buttonID" class="btn btn-success">Save</button>
                            </div>
                          </div> -->
                        </div>
                      </table>
                    </div>
                    <div class="modal-footer">                         
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>   
                      <button type="submit" id="buttonID" class="btn btn-success">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <br/>
            <!-- tabel body -->
            <div class="table-responsive">
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr >
                    <th style="width: 1%">No </th>
                    <th >Username </th>
                    <th >Name </th>
                    <th >Phone Number</th>
                    <th >Role </th>
                    <th >Status </th>
                    <th style="width: 20%"><span class="nobr">Action</span>
                    </th>
                  </tr>
                </thead>

                <tbody>
            		<?php $i = 1; ?>
                  <?php foreach ($member as $data): ?>
                  <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $data->username; ?></td>
                      <td><?php echo $data->name; ?></td>
                      <td><?php echo $data->phone; ?></td>
                      <td><?php echo $data->user_role; ?></td>
                      <td><?php 
                          if($data->user_status === '0'){
                              echo "Not Aktive";
                          }else{
                              echo"Active";
                          }
                      ?> 
                      </td>
                      <td>
                          <a data-toggle="modal" data-target=".aktivasi-modal-md" id="<?php echo $data->user_id; ?>"" class="btn btn-primary btn-xs aktivasi_button" data-toggle='modal'><i class='glyphicon glyphicon-check'></i> Aktivasi</a>
                          <a href="<?php echo site_url('backend/member/edit') . '/' . $data->user_id; ?>" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                          <a onClick="return confirm('Are you sure to delete this?');" href="<?php echo site_url('backend/member/delete') . '/' . $data->user_id; ?>" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                      </td>
                  </tr>
                  <?php $i++; ?>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
    </div>
</div>


<div class="modal fade aktivasi-modal-md" id="aktivasi_member" tabindex="-1" role="dialog" aria-hidden="true"> </div>

<script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatable').DataTable({
      "columnDefs": [
          {"orderable": false, "targets": [6]},
      ]
    });
    
});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#username').blur(function(){
      var username = $('#username').val();
      var data = 'username='+username;
      $.ajax({
        type: "POST",
        data: data,
        url: "<?php echo base_url('backend/member/check') ?>",
        dataType: "json",
        success: function(result){
          if( result.status == 1)
          {
            alert('Username already exist! Please try another username');
            $('#username').val('');
            $('#username').focus();
          }
        }
      });
    });
    $('#buttonID').click(function(e){
      var password = $('#password').val();
      var confirmpassword = $('#confirmpassword').val();
      if (password != confirmpassword){
        alert('Please enter the same password as above');
        e.preventDefault();
      }else{
        
      }
    });

    $('.aktivasi_button').click(function(){
      var user_id = $(this).attr('id');
      var info = 'user_id='+user_id;
      $.ajax({
        type: "POST",
        url: "<?php echo site_url('backend/Member/aktivasi') ?>",
        data: info,
        success: function(html){
          $('#aktivasi_member').html(html);
        }
      });
    }); 
  });
</script>