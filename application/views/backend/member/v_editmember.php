<div class="">
	<div class="page-title">
        <div class="title_left">
            <h3></h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for..."/>
                    <span class="input-group-btn">
                         <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Daftar Member <small><i>Daily Report System</i></small></h2>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
            <form method="POST" id="userForm" action="<?php echo base_url('backend/member/save_member');?>" class="form-horizontal"  enctype="multipart/form-data" role="form">
              <table class="table">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-4 control-label">Username</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" id="username" name="username" value="<?php echo $member->username;?>" required disabled>
                      <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $member->user_id;?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Full Name</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $member->name;?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Phone</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $member->phone;?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Role</label>
                    <div class="col-md-6">
                      <select class="form-control" name="role" id="user_role" tabindex="-1" required>
                        <option value="web developer" <?php if ($member->user_role == 'web developer') {echo "selected";} ?>>Web Developer</option>
                        <option value="web designe" <?php if ($member->user_role == 'web designe') {echo "selected";} ?>>Web Designe</option>
                        <option value="human resource" <?php if ($member->user_role == 'human resource') {echo "selected";} ?>>Human Resource</option>
                        <option value="trainee" <?php if ($member->user_role == 'trainee') {echo "selected";} ?>>Trainee</option>
                      </select> 
                    </div>
                  </div>
                  <div class="form-actions fluid">
                    <div class="col-md-6 col-md-offset-4 pull-left">
                      <button type="button" class="btn btn-default" onClick="history.go(-1);return true;">Cancel</button>
                      <button type="submit" class="btn btn-success">Save</button>
                    </div>
                  </div>
                </div>
              </table>
            </form>
          </div>
        </div>
    </div>
    </div>
</div>