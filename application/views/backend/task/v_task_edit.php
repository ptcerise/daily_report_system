<style>
    .datepicker {
      z-index: 1600 !important; /* has to be larger than 1050 */
    }
</style>

<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Edit Task</h4>
    </div>
    <div class="modal-body">
      <form method="POST" id="userForm" class="form-horizontal" action="<?php echo base_url('backend/Task/save_edit/'.$task->task_id);?>"  enctype="multipart/form-data" role="form">
        <table class="table">
          <div class="col-md-12">
            <div class="form-group">
              <label class="col-md-4 control-label">Task Name</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="task_name" name="task_name" placeholder="Task Name" value="<?php echo $task->task_name;?>" required>
                <input type="hidden" class="form-control" id="task_id" name="task_id" value="<?php echo $task->task_id;?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Due Date</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="task_deadline" name="task_deadline" placeholder="Due Date" value="<?php echo $deadline;?>" required>
              </div>
            </div>
            <div class="form-actions fluid">
              <div class="col-md-6 col-md-offset-4 pull-left">
                <button data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" id="buttonID" class="btn btn-success">Save</button>
              </div>
            </div>
          </div>
        </table>
      </form>
    </div>
  </div>
</div>
<!-- 
<script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
    $('#project_start').datepicker({
     format: 'dd-mm-yyyy'
    });   
    $('#deadline').datepicker({
     format: 'dd-mm-yyyy'
    });  
</script> -->