<style>
  .datepicker {
    z-index: 1600 !important; /* has to be larger than 1050 */
  }
</style>

<div class="">
  <div class="page-title">
    <div class="title_left">
        <h3></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <!-- <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for..."/>
                <span class="input-group-btn">
                     <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div> -->
        </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <button type="button" class="btn btn-default" onClick="history.go(-1);return true;"> <i class="glyphicon glyphicon-hand-left"></i> Back</button>
          <ul class="nav navbar-right panel_toolbox">
            <?php if ($this->session->userdata('user_level') == '1'){
              echo '<button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-md"> <i class="fa fa-edit m-right-xs"></i> Add Task</button>';
            }else{}
            ?>
          </ul>
          <!-- Modal Body -->
          <div class="modal fade bs-example-modal-md" id="addTask" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Add New Task</h4>
                </div>
                <div class="modal-body">
                  <form method="POST" id="userForm" class="form-horizontal" action="<?php echo base_url('backend/task/addtask');?>"  enctype="multipart/form-data" role="form">
                    <table class="table">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="col-md-4 control-label">Task Name</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" id="task_name" name="task_name" placeholder="Task Name" value="" required>
                            <input type="hidden" class="form-control" id="project_id" name="project_id" value="<?php echo $project->project_id;?>" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4 control-label">Due Date</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" id="task_deadline" name="task_deadline" placeholder="Due Date" value="" required>
                          </div>
                        </div>
                        <div class="form-actions fluid">
                          <div class="col-md-6 col-md-offset-4 pull-left">
                            <button data-dismiss="modal" class="btn btn-default">Cancel</button>
                            <button type="submit" id="buttonID" class="btn btn-success">Save</button>
                          </div>
                        </div>
                      </div>
                    </table>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <!--Close Modal Body -->
          <div class="x_title">
            <div>
              <h2>Project Name : <?php echo strtoupper($project->project_name); ?> </h2>
              <div class="clearfix"></div>
            </div>
            <div>
              <small><i>Start Date : <?php echo $start;?> - Due Date : <?php echo $deadline;?></i></small>
            </div>
          </div>

          <div class="x_content">
            <?php echo $this->session->flashdata('task');?>
            <div>
              Desciption project......
            </div>
            <br>
            <div>
              <?php foreach ($task as $data): 
                  $date =  $data->task_deadline;            
                  $newdate = date('d-m-Y', strtotime(str_replace('/', '-', $date)));
                  $as =  $data->task_deadline;            
                  $newdeadline = date('d-m-Y', strtotime(str_replace('/', '-', $as)));
              ?>              
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel">
                  <a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $data->task_id;?>" aria-expanded="false" aria-controls="collapseOne">
                    <h4 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i> <?php echo $data->task_name;?></h4>
                    <small class="d"><i>Due Date : <?php echo $newdeadline;?></i></small>
                  </a>
                  <div id="collapse<?php echo $data->task_id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <div  class="col-md-9 col-xs-12">
                        <ul class="list-group checked-list-box checklist">
                          <li class="list-group-item">Cras justo odio</li>
                        </ul>
                      </div>
                      <div  class="col-md-3 col-xs-12">
                        <h4><b>Actions</b></h4>
                        <?php if ($this->session->userdata('user_level') == '1'){
                          echo '<a onClick="return confirm_delete()" href="'.site_url('backend/Task/delete') . '/' . $data->task_id.'" class="btn btn-danger b" > <i class="fa fa-trash-o"></i> Delete Task</a><div class="clearfix"></div>';
                          echo '<a href="#" class="b btn btn-warning button_edit" data-toggle="modal" id="'.$data->task_id.'" data-target=".edit-modal-md"> <i class="fa fa-edit m-right-xs"></i> Edit Task</a><div class="clearfix"></div>';
                        }else{}
                        ?>
                        <br>
                        <button type="button" class="btn btn-default b"> <i class="fa fa-edit m-right-xs"></i> Add Detail Task</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>              
              <?php endforeach; ?>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade edit-modal-md" id="edit_task" tabindex="-1" role="dialog" aria-hidden="true"> </div>
<script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
function confirm_delete() {
  return confirm('are you sure?');
}
  $(document).ready(function(){
    $('#datatable').DataTable({
      "columnDefs": [
          {"orderable": false, "targets": [5]},
      ]
    });
    $('#task_deadline').datepicker({
     format: 'dd-mm-yyyy'
    }); 
    $('.button_edit').click(function(){
      var task_id = $(this).attr('id');
      var info = 'task_id='+task_id;
      $.ajax({
        type: "POST",
        url: "<?php echo site_url('backend/Task/edit') ?>",
        data: info,
        success: function(html){
          $('#edit_task').html(html);
        }
      });
    });  
});
  $(function () {
    $('.list-group.checked-list-box .list-group-item').each(function () {
        
        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };
            
        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
          

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {
            
            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }
            
            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });
    
    $('#get-checked-data').on('click', function(event) {
        event.preventDefault(); 
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function(idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
});
</script>