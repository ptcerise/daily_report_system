<style>
    .datepicker {
      z-index: 1600 !important; /* has to be larger than 1050 */
    }
</style>

<div class="">
  <div class="page-title">
        <div class="title_left">
            <h3></h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <!-- <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for..."/>
                    <span class="input-group-btn">
                         <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div> -->
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">

            <div class="x_title">
              <h2>List Project <small><i> Daily Report System</i></small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <?php if ($this->session->userdata('user_level') == '1'){
                  echo '<button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-md"> <i class="fa fa-edit m-right-xs"></i> Add Project</button>';
                }else{}
                ?>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <div class="modal fade bs-example-modal-md" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                      </button>
                      <h4 class="modal-title" id="myModalLabel">Add New Project</h4>
                    </div>
                    <div class="modal-body">
                      <form method="POST" id="userForm" class="form-horizontal" action="<?php echo base_url('backend/project/add');?>"  enctype="multipart/form-data" role="form">
                        <table class="table">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-4 control-label">Project Name</label>
                              <div class="col-md-6">
                                <input type="text" class="form-control" id="project_name" name="project_name" placeholder="Project Name" value="" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-4 control-label">Project Start</label>
                              <div class="col-md-6">
                                <input type="text" class="form-control" id="project_start" name="project_start" placeholder="Start" value="" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-4 control-label">Project Deadline</label>
                              <div class="col-md-6">
                                <input type="date" class="form-control" id="project_deadline" name="project_deadline" placeholder="Deadline" value="" required>
                              </div>
                            </div>
                            <div class="form-actions fluid">
                              <div class="col-md-6 col-md-offset-4 pull-left">
                                <button data-dismiss="modal" class="btn btn-default">Cancel</button>
                                <button type="submit" id="buttonID" class="btn btn-success">Save</button>
                              </div>
                            </div>
                          </div>
                        </table>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <br/>
              <!-- start project list -->
              <div class="table-responsive">
              <?php echo $this->session->flashdata('project');?>
                <table id="datatable" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 1%">No</th>
                      <th style="width: 20%">Project Name</th>
                      <!-- <th>Team Members</th> -->
                      <th>Start Project</th>
                      <th>Deadline</th>
                      <th style="width: 20%">Project Progress</th>
                      <!-- <th>Status</th> -->
                      <th style="width: 20%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($project as $data): 
                      $date =  $data->project_start;            
                      $newdate = date('d-m-Y', strtotime(str_replace('/', '-', $date)));
                      $as =  $data->project_deadline;            
                      $newdeadline = date('d-m-Y', strtotime(str_replace('/', '-', $as)));
                      ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $data->project_name; ?></td>
                      <td><?php echo $newdate; ?></td>
                      <td><?php echo $newdeadline; ?></td>
                      <td class="project_progress">
                        <div class="progress progress_sm">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="<?php echo $data->project_progress; ?>"></div>
                        </div>
                        <small><?php echo $data->project_progress; ?>% Complete</small>
                      
                      </td>
                      <td>
                         <a href="<?php echo base_url('backend/Task/detail/'.$data->project_id);?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Task </a>
                          <?php if($this->session->userdata('user_level') == '1'){
                          echo '
                                <a href="#" class="btn btn-info btn-xs button_edit" data-toggle="modal" id="'.$data->project_id.'" data-target=".edit-modal-md"><i class="fa fa-pencil"></i> Edit </a>
                                <a onClick="return confirm_delete()" href="'.site_url('backend/project/delete') . '/' . $data->project_id.'" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          ';
                        }
                        ?>
                      </td>
                    </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <!-- end project list -->
            </div>
          </div>
      </div>
    </div>
</div>

<div class="modal fade edit-modal-md" id="edit_project" tabindex="-1" role="dialog" aria-hidden="true"> </div>

<script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
  function confirm_delete() {
    return confirm('are you sure?');
  }

  $(document).ready(function(){
    $('#datatable').DataTable({
      "columnDefs": [
          {"orderable": false, "targets": [5]},
      ]
    });
    $('#project_start').datepicker({
     format: 'dd-mm-yyyy'
    });   
    $('#project_deadline').datepicker({
     format: 'dd-mm-yyyy'
    });  
});
  $('#edit_project').on('show.bs.modal', function () {
    $('#start').datepicker('remove');
    $('#start').datepicker({
     format: 'dd-mm-yyyy'
    });   
    $('#deadline').datepicker({
     format: 'dd-mm-yyyy'
    }); 
});  
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#project_name').blur(function(){
      var project_name = $('#project_name').val();
      var data = 'project_name='+project_name;
      $.ajax({
        type: "POST",
        data: data,
        url: "<?php echo base_url('backend/Project/check') ?>",
        dataType: "json",
        success: function(result){
          if( result.status == 1)
          {
            alert('Project already exist! Please try another name');
            $('#project_name').val('');
            $('#project_name').focus();
          }
        }
      });
    });
    
    $('.button_edit').click(function(){
      var project_id = $(this).attr('id');
      var info = 'project_id='+project_id;
      $.ajax({
        type: "POST",
        url: "<?php echo site_url('backend/Project/edit') ?>",
        data: info,
        success: function(html){
          $('#edit_project').html(html);
        }
      });
    });  
  });
</script>

