<div class="">
	<div class="page-title">
    <div class="title_left">
        <h3></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <!-- <input type="text" class="form-control" placeholder="Search for..."/>
                <span class="input-group-btn">
                     <button class="btn btn-default" type="button">Go!</button>
                </span> -->
            </div>
        </div>
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Setting Email Address <small><i>Daily Report System</i></small></h2>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <form method="POST" id="userForm" action="" class="form-horizontal"  enctype="multipart/form-data" role="form">
            <table class="table">
              <div class="col-md-12">
                <div class="form-group">
                  <div class="col-md-7">
                    <div class="form-group">
                      <label class="col-md-4 control-label">Email Address</label>
                      <div class="col-md-6">
                        <input type="text" class="form-control" id="username" name="username" value="" required>
                      </div> 
                    </div>
                    <div class="form-group">         
                      <div class="form-actions fluid">
                        <div class="col-md-6 col-md-offset-4 pull-left">
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                      </div>                      
                    </div>  
                  </div>
                  <div class="col-md-5">
                    <p>
                      This email is used to receive messages sent by users. 
                      If you want to replace with another email please replace email address in the form.
                    </p>
                    <p>
                      If there is more than 1 email recipients separate between emails with commas (,)<br/>
                      Example: email_first@mail.com, email_second@mail.com, etc
                    </p>
                  </div>
                </div>
              </div>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>