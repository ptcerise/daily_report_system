<div class="">
	<div class="page-title">
        <div class="title_left">
            <h3></h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for..."/>
                    <span class="input-group-btn">
                         <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Report Project <small><i>Daily Report System</i></small></h2>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
            <form method="POST" id="userForm" action="" class="form-horizontal"  enctype="multipart/form-data" role="form">
              <table class="table">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-4 control-label">Project Name</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" id="username" name="username" value="" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Task</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" id="name" name="name" value="" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Constraint</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" id="phone" name="phone" value="" required>
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-md-4 control-label">Progress</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" id="phone" name="phone" value="" required>
                    </div>
                  </div>
                  <div class="form-actions fluid">
                    <div class="col-md-6 col-md-offset-4 pull-left">
                      <button type="submit" class="btn btn-success">Send</button>
                    </div>
                  </div>
                </div>
              </table>
            </form>
          </div>
        </div>
    </div>
    </div>
</div>