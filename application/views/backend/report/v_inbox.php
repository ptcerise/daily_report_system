<div class="">
	<div class="page-title">
        <div class="title_left">
            <h3></h3>
        </div>

        <div class="title_right">
            <!-- <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for..."/>
                    <span class="input-group-btn">
                         <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div> -->
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Inbox <small><i> Daily Report System</i></small></h2>
              <div class="clearfix"></div>
            </div>            

            <div class="x_content">
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th style="width: 1%">No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Tiger Nixon</td>
                    <td>Edinburgh@yahoo.com</td>
                    <td>uji cobaaaaaaaaa</td>
                    <td>2011/04/25</td>
                    <td>
                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Donna Snider</td>
                    <td>donna@gmail.vom</td>
                    <td>teeeeesssssssss</td>
                    <td>2011/01/25</td>
                    <td>
                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatable').DataTable({
      "columnDefs": [
          {"orderable": false, "targets": [5]},
      ]
    });
    
});
</script>