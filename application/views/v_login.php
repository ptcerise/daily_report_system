<!DOCTYPE html>
<html>
<head>
    <title>Login | Daily Report System</title>
    <meta name="viewport" content="width=1360px, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.css">
</head>
<body>
    <section>
        <p class="welcome">Daily Report System</p>
        <div class="container">
            <form class="form-signin" role="form" method="POST" action="<?php echo base_url('login/proccess');?>">
            <?php echo $this->session->flashdata('login');?>
                <input type="text" name="username" class="form-control" placeholder="Username" autocomplete="off" required autofocus>
                <input type="password" name="password" class="form-control" placeholder="Password" required autocomplete="off">
                <button class="btn btn-success" type="submit">Sign in</button>
                <a href="<?php echo site_url('register'); ?>" class="text-center"> <i>Create Account</a>
            </form>
            <div class="clearfix"></div>
            <br />
            <div>
              <p style="text-align: center;"><strong>Copyright &copy; <?php echo date('Y'); ?> Daily Report System | developed by <a href="http://ptcerise.com" target="blank">Cerise</a>.</strong> All rights reserved.</p>
            </div>
        </div>
    </section>
    <footer>
        <div class="line"></div>
    </footer>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>