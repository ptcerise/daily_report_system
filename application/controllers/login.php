<?php
class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        session_start();
    }
    
    function index() {
        $this->load->view('v_login');
    }

    function proccess() {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $check = $this->access->readtable('user', '', array('username' => $username, 'password' => $password));

        if($check->num_rows() > 0){
           if($check->row()->user_status == '1'){
             $login = array(
                            'ready_login' => TRUE,
                            'user_id' => $check->row()->user_id,
                            'username' => $check->row()->username,
                            'name' => $check->row()->name,
                            'user_image' => $check->row()->user_image,
                            'password' => $check->row()->password,
                            'user_level' => $check->row()->user_level,                            
                            'user_role' => $check->row()->user_role,
                            );
            $this->session->set_userdata($login);
            $user_id = $this->session->userdata('user_id');
            redirect('backend/Dashboard');
           }else{
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Your acoount is not yet active.</div>';
            $this->session->set_flashdata('login', $notif);
            redirect('login');
           }
        }
        else{
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Username or password is wrong.</div>';
            $this->session->set_flashdata('login', $notif);
            redirect('login');
        }
    }

    function logout() {        
        $this->session->sess_destroy();
        redirect('login');
    }
}
