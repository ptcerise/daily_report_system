<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

    public function index()	{
		$this->load->view('v_register');
	}

	public function registration() {		
        if (empty($_POST)) {
            $this->load->view('v_register');
        } 
        else{
            if($this->input->post('role') == 'web developer'){
                $level = '2';
            }else if($this->input->post('role') == 'web designe'){
                 $level = '2';
            }else if($this->input->post('role') == 'human resource'){
                 $level = '3';
            }else{
                $level = '4';
            }

        	$user = array(
                'username'=>$this->input->post('username'),
                'name'=>$this->input->post('name'),
                'phone'=>$this->input->post('phone'),
                'user_level'=> $level,
                'user_role'=>$this->input->post('role'),
                'user_image'=> 'user.png',
                'password'=>md5($this->input->post('password'))
            );
        $this->access->inserttable('user',$user);  
        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Registration user successful, Please login</div>';
        $this->session->set_flashdata('info_user', $notif);      
        redirect('register');
        }
    }

    function check() {
        $username = $this->input->post('username');
        $check = $this->access->readtable('user', '', array('username' => $username))->num_rows();
        if( $check > 0 )
        {
            $status = 1;
        } else {
            $status = 0;
        }

        $data = array('status' => $status);
        echo json_encode($data);
    }
}

