<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }

	function index() {        
        $data['project'] = $this->db->query("select * from project")->result();
		$view['content'] = $this->load->view('backend/project/v_project',$data,TRUE);
		$this->load->view('backend/v_master', $view);
	}

    function check() {
        $project_name = $this->input->post('project_name');
        $check = $this->access->readtable('project', '', array('project_name' => $project_name))->num_rows();
        if( $check > 0 )
        {
            $status = 1;
        } else {
            $status = 0;
        }

        $data = array('status' => $status);
        echo json_encode($data);
    }
    

    public function add() {
        $start = $this->input->post('project_start');
        $newstart = date('Y-m-d', strtotime(str_replace('/', '-', $start)));
        $deadline = $this->input->post('project_deadline');
        $newdeadline = date('Y-m-d', strtotime(str_replace('/', '-', $deadline)));
        $addproject = array(
            'project_name'=>$this->input->post('project_name'),
            'project_start'=> $newstart,
            'project_deadline'=>$newdeadline,
        );
        $this->access->inserttable('project',$addproject);        
        redirect('backend/Project');
    }

    // public function view() {       
    //     $project_id= $this->input->post('project_id');
    //     $data['project'] = $this->access->readtable('project','',array('project_id'=>$project_id))->row(); 
    //     $this->load->view('backend/project/v_project_view',$data);
    // }

    public function edit() {       
        $project_id = $this->input->post('project_id');
        $data['project'] = $this->access->readtable('project','',array('project_id'=>$project_id))->row(); 
        $date =  $data['project']->project_start;
        $newdate = date('d-m-Y', strtotime(str_replace('/', '-', $date)));
        $data['start'] = $newdate;
        $a =  $data['project']->project_deadline;
        $newdeadline = date('d-m-Y', strtotime(str_replace('/', '-', $a)));
        $data['deadline'] = $newdeadline;
        $this->load->view('backend/project/v_project_edit',$data);
    }

    public function save_edit(){
        $project_id= $this->input->post('project_id');
        $fill_project = array(
                            'project_name'=>$this->input->post('project_name'),
                            'project_start'=>$this->input->post('project_start'),
                            'project_deadline'=>$this->input->post('project_deadline'),
                            );
        $this->access->updatetable('project',$fill_project,array('project_id'=>$project_id));

        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Edit project successful.</div>';
        $this->session->set_flashdata('project', $notif);
        redirect('backend/Project');
    }

	public function delete($project_id) {
        $this->access->deletetable('project',array('project_id'=>$project_id));
        
        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>Delete project successful.</div>';
        $this->session->set_flashdata('project', $notif);
        redirect('backend/Project');
        break;
    }
}