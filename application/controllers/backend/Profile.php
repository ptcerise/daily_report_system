<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct() {
        parent::__construct();
    }

	function index() {
		$user_id = $this->session->userdata('user_id');
		$data['user'] = $this->access->readtable('user','',array('user_id'=>$user_id))->row();
		$view['content'] = $this->load->view('backend/profile/v_profile',$data,TRUE);
		$this->load->view('backend/v_master', $view);
	}

	public function account($user_id) {
		$user = array(
					'username' => $this->input->post('username'),
					'name' => $this->input->post('name'),
					'phone' => $this->input->post('phone')
					);
		$this->db->where('user_id', $user_id);
		$this->db->update('user', $user);
		$this->session->set_userdata($user);
		$this->session->set_flashdata('account','Your profile has been changed. Please re-login.');
		redirect('backend/profile');
	}

	public function password($user_id) {
		$current = md5($this->input->post('current'));
		$new = md5($this->input->post('new'));

		$check = $this->access->readtable('user','password',array('user_id'=>$user_id))->row()->password;
		
		if($check == $current){
			$this->access->updatetable('user',array('password'=>$new),array('user_id'=>$user_id));
			$this->session->set_flashdata('info_login','Change password success. Pleas re-login.');
			redirect('backend/dashboard/logout');
		} else {
			$this->session->set_flashdata('password',"Current password wrong.");
			redirect('backend/profile');
		}
	}

	function change_avatar() {
		$avatar = $_FILES['avatar']['name'];
		$break = explode('.', $avatar);
		$ext = $break[count($break) - 1];
		$date = date('dmYHis');
		$name_url = 'avatar-'.$date.'.'.$ext;
		$path = './assets/upload/user_image';

		if( ! file_exists( $path ) )
		{
			$create = mkdir($path, 0777, TRUE);
			$createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
			if( ! $create || ! $createThumb )
				return;
		}
		
		$this->piclib->get_config($name_url, $path);
		if( $this->upload->do_upload('avatar') )
		{
			$image = array('upload_data' => $this->upload->data());
			$source_path = $image['upload_data']['full_path'];
			$width = $image['upload_data']['image_width'];
			$height = $image['upload_data']['image_height'];
			
			if( $width < 200 || $height < 200 )
			{
				unlink( realpath( APPPATH.'../assets/upload/user_image/'.$name_url ));
				unlink( realpath( APPPATH.'../assets/upload/user_image/thumbnail/'.$name_url ));
				$notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>Image must be at least 200 x 200.</div>';
			}else{
				$orientation = $this->piclib->orientation($source_path);
				if( $orientation != 'square' )
				{
					unlink( realpath( APPPATH.'../assets/upload/user_image/'.$name_url ));
					unlink( realpath( APPPATH.'../assets/upload/user_image/thumbnail/'.$name_url ));
					$notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>Image must be square.</div>';		
				}else{
					$this->piclib->resize_image($source_path, $width, $height, 200, 200);
					if( $this->image_lib->resize() )
					{
						$this->image_lib->clear();
						$this->piclib->resize_image($source_path, $width, $height, 100, 100, $path.'/thumbnail');
						$this->image_lib->resize();
					}
					
					$slide = array(
									'user_image'=>$name_url
									);
					$this->access->updatetable('user',$slide, array('user_id'=>$this->input->post('id')));
										
					$notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>Change avatar successful.</div>';	
				}
			}
		}
		else{
			$error = $this->upload->display_errors();
			$notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';	
		}

		$this->session->set_flashdata('alert', $notif);
		redirect('backend/Profile');
	}
}