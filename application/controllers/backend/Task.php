<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Task extends CI_Controller {

    function index() {
        redirect('backend/Project');
    }

	function detail( $project_id) {
        $data['task'] = $this->db->query("select * from task where project_id = $project_id")->result();
        $data['project'] = $this->access->readtable('project','',array('project_id'=>$project_id))->row(); 
        $date =  $data['project']->project_start;
        $newdate = date('d-m-Y', strtotime(str_replace('/', '-', $date)));
        $data['start'] = $newdate;
        $a =  $data['project']->project_deadline;
        $newdeadline = date('d-m-Y', strtotime(str_replace('/', '-', $a)));
        $data['deadline'] = $newdeadline;
		$view['content'] = $this->load->view('backend/task/v_task',$data,TRUE);
		$this->load->view('backend/v_master', $view);
	}

    public function addtask() {
        $deadline = $this->input->post('task_deadline');
        $newdeadline = date('Y-m-d', strtotime(str_replace('/', '-', $deadline)));
        $addtask = array(
            'project_id'=>$this->input->post('project_id'),
            'task_name'=>$this->input->post('task_name'),
            'task_deadline'=>$newdeadline,
        );
        $this->access->inserttable('task',$addtask);        
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit() {       
        $task_id = $this->input->post('task_id');
        $data['task'] = $this->access->readtable('task','',array('task_id'=>$task_id))->row(); 
        $a =  $data['task']->task_deadline;
        $newdeadline = date('d-m-Y', strtotime(str_replace('/', '-', $a)));
        $data['deadline'] = $newdeadline;
        $this->load->view('backend/task/v_task_edit',$data);
    }

     public function save_edit(){
        $task_id= $this->input->post('task_id');
        $fill_task = array(                                                      
                            'task_name'=>$this->input->post('task_name'),
                            'task_deadline'=>date('Y-m-d', strtotime($this->input->post('task_deadline'))),
                            );
        $this->access->updatetable('task',$fill_task,array('task_id'=>$task_id));

        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Edit task successful.</div>';
        $this->session->set_flashdata('task', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    function delete($task_id){
        $this->access->deletetable('detail_task',array('task_id'=>$task_id));
        $this->access->deletetable('task',array('task_id'=>$task_id));
        
        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>Delete task successful.</div>';
        $this->session->set_flashdata('task', $notif);
        redirect($_SERVER['HTTP_REFERER']);
        break;
    }
}