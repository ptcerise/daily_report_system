<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {
	public function __construct() {
        parent::__construct();
         $this->autentikasi_login();
    }

    public function autentikasi_login() {
        if ($this->session->userdata('ready_login') == TRUE) {
            if ($this->session->userdata('user_level') !== '1') {
                redirect(base_url().'login');
            }
        }else{
            redirect(base_url().'backend/dashboard');
        }
    }

	function index() {
		$data['member'] = $this->db->query("select * from user where user_level NOT IN (1)")->result();
		$view['content'] = $this->load->view('backend/member/v_member',$data,TRUE);
		$this->load->view('backend/v_master', $view);
	}

    function edit($user_id) {
        $data['member'] = $this->access->readtable('user','',array('user_id'=>$user_id))->row();
        $view['content'] = $this->load->view('backend/member/v_editmember',$data,TRUE);
        $this->load->view('backend/v_master', $view);
    }

    public function aktivasi() {       
        $user_id= $this->input->post('user_id');
        $data['member'] = $this->access->readtable('user','',array('user_id'=>$user_id))->row(); 
        $this->load->view('backend/member/v_aktivasi',$data);
    }

    function check() {
        $username = $this->input->post('username');
        $check = $this->access->readtable('user', '', array('username' => $username))->num_rows();
        if( $check > 0 )
        {
            $status = 1;
        } else {
            $status = 0;
        }

        $data = array('status' => $status);
        echo json_encode($data);
    }
    
    function save_member () {
        $user_id= $this->input->post('user_id');
        $fill_user = array(
                            'name'=>$this->input->post('name'),
                            'phone'=>$this->input->post('phone'),
                            'user_Level'=>$this->input->post('role')
                            );
        $this->access->updatetable('user',$fill_user,array('user_id'=>$user_id));
        $username = $this->access->readtable('user','username',array('user_id'=>$user_id))->row()->username;

        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Edit user successful.</div>';
        $this->session->set_flashdata('info_user', $notif);
        redirect('backend/Member');
    }

    function save_status () {
        $user_id= $this->input->post('user_id');
        $fill_user = array(
                            'user_status'=>$this->input->post('user_status')
                            );
        $this->access->updatetable('user',$fill_user,array('user_id'=>$user_id));
        $username = $this->access->readtable('user','username',array('user_id'=>$user_id))->row()->username;

        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Aktivasi successful.</div>';
        $this->session->set_flashdata('info_user', $notif);
        redirect('backend/Member');
    }


	function add() {        
        $user = array(
            'username'=>$this->input->post('username'),
            'name'=>$this->input->post('name'),
            'phone'=>$this->input->post('phone'),
            'user_Level'=>$this->input->post('role'),
            'password'=>md5($this->input->post('password'))
        );
        $this->access->inserttable('user',$user);        
        redirect('backend/Member');
    }

	public function delete($user_id) {
        $this->access->deletetable('user',array('user_id'=>$user_id));
        
        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>Delete user successful.</div>';
        $this->session->set_flashdata('info_user', $notif);
        redirect('backend/Member');
        break;
    }
}