<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

	function index() {
		$view['content'] = $this->load->view('backend/report/v_report','',TRUE);
		$this->load->view('backend/v_master', $view);
	}

    function inbox() {
        $view['content'] = $this->load->view('backend/report/v_inbox','',TRUE);
        $this->load->view('backend/v_master', $view);
    }

     function setting() {
        $view['content'] = $this->load->view('backend/report/v_setting','',TRUE);
        $this->load->view('backend/v_master', $view);
    }
}