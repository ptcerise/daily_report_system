<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
        parent::__construct();
    }

	function index() {
        if ($this->session->userdata('user_level') !== '1'){
            $page = "v_home1";
        }else{
            $page = "v_home";
        }
		$view['content'] = $this->load->view('backend/home/'.$page,'',TRUE);
		$this->load->view('backend/v_master', $view);
	}
}