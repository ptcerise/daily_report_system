<?php

class M_user extends CI_Model {

    private $table = "user";

    function cek($username, $password) {
        $this->db->where("username", $username);
        $this->db->where("password", $password);
        return $this->db->get("user");
    }

    function semua() {
        return $this->db->get("user");
    }

    function cekKode($kode) {
        $this->db->where("username", $kode);
        return $this->db->get("user");
    }

    function cekId($kode) {
        $this->db->where("user_id", $kode);
        return $this->db->get("user");
    }


    public function profile_admin($id_admin) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $id_admin);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function daftarmember() {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_level', 'member');
        $this->db->order_by('user_id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }
    
    function getLoginData($usr, $psw) {
        $u = mysql_real_escape_string($usr);
        $p = md5(mysql_real_escape_string($psw));
        $q_cek_login = $this->db->get_where('users', array('username' => $u, 'password' => $p));
        if (count($q_cek_login->result()) > 0) {
            foreach ($q_cek_login->result() as $qck) {
                foreach ($q_cek_login->result() as $qad) {
                    $sess_data['logged_in'] = 'aingLoginWebYeuh';
                    $sess_data['user_id'] = $qad->user_id;
                    $sess_data['username'] = $qad->username;
                    $sess_data['name'] = $qad->name;
                    $sess_data['phone'] = $qad->phone;
                    $sess_data['rid'] = $qad->rid;
                    $this->session->set_userdata($sess_data);
                }
                redirect('dashboard');
            }
        } else {
            $this->session->set_flashdata('result_login', '<br>Username atau Password yang anda masukkan salah.');
            header('location:' . base_url() . 'login');
        }
    }

    function update($id, $info) {
        $this->db->where("user_id", $id);
        $this->db->update("user", $info);
    }

    function simpan($info) {
        $this->db->insert("user", $info);
    }

    function hapus($kode) {
        $this->db->where("user_id", $kode);
        $this->db->delete("user");
    }
}
